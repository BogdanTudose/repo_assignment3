
const FIRST_NAME = "BOGDAN";
const LAST_NAME = "TUDOSE";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
class Employee {
    
    constructor (name, surname, salary)
    {
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }

    getDetails() 
    {
        return (this.name +' '+ this.surname +' '+ this.salary);
    }
}

class SoftwareEngineer extends Employee {
    constructor(name,surname,salary, experience = 'JUNIOR')
    {
        super(name,surname,salary);
        this.experience=experience;
    }

    getDetails()

    {
        return (this.name +' '+ this.surname +' '+ this.salary);
        
    }
    
    applyBonus()
    {
        switch (this.experience){

         case 'JUNIOR' :
         {
             return (this.salary*1.1);
             break;
         }

         case 'MIDDLE' :
         {
             return (this.salary*1.15);
            break;
         }

         case 'SENIOR' :
        {
            return (this.salary*1.2);
            break;
        }

        default :
        {
            return (this.salary*1.1);
            
        }

        }

    }
   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

